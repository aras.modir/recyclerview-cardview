package com.example.recycler_cardview_part1;

public class ExampleItem {
    private int mImageSource;
    private String mText1;
    private String mText2;

    public ExampleItem(int imageSource, String text1, String text2) {
        mImageSource = imageSource;
        mText1 = text1;
        mText2 = text2;
    }

    public void changeText1(String text) {
        mText1 = text;
    }

    public String getmText1() {
        return mText1;
    }

    public String getmText2() {
        return mText2;
    }

    public int getmImageSource() {
        return mImageSource;
    }

}
