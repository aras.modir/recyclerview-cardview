package com.example.recycler_cardview_part1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private ExampleAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private ArrayList<ExampleItem> mExamplelist;
    private Button buttonInsert, buttonRemove;
    private EditText editTextInsert, editTextRemove;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText editText = findViewById(R.id.search);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });

        createExampleList();
        buildRecyclerView();
        setButtons();
    }

    private void filter(String text) {
        ArrayList<ExampleItem> filteredList = new ArrayList<>();
        for (ExampleItem item : mExamplelist) {
            if (item.getmText1().toLowerCase().contains(text.toLowerCase())){
                filteredList.add(item);
            }
        }
        mAdapter.filterList(filteredList);
    }

    public void insertItem(int position) {
        mExamplelist.add(position, new ExampleItem(R.drawable.ic_android, "New Item At Position" + position, "This is Line 2"));
        mAdapter.notifyItemInserted(position);
    }

    public void changeItem(int position, String text) {
        mExamplelist.get(position).changeText1(text);
        mAdapter.notifyItemChanged(position);
    }

    public void removeItem(int position) {
        mExamplelist.remove(position);
        mAdapter.notifyItemRemoved(position);
    }

    private void buildRecyclerView() {

        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new ExampleAdapter(mExamplelist);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new ExampleAdapter.onItemClickListener() {
            @Override
            public void onItemClick(int position) {
                changeItem(position, "Clicked");
            }

            @Override
            public void onDeleteClick(int position) {
                removeItem(position);
            }

        });
    }

    private void createExampleList() {
        mExamplelist = new ArrayList<>();
        mExamplelist.add(new ExampleItem(R.drawable.ic_android, "One", "Line 1"));
        mExamplelist.add(new ExampleItem(R.drawable.ic_audiotrack, "Two", "Line 2"));
        mExamplelist.add(new ExampleItem(R.drawable.ic_wb_sunny, "Three", "Line 3"));
        mExamplelist.add(new ExampleItem(R.drawable.ic_android, "Four", "Line 4"));
        mExamplelist.add(new ExampleItem(R.drawable.ic_audiotrack, "Five", "Line 5"));
        mExamplelist.add(new ExampleItem(R.drawable.ic_wb_sunny, "Six", "Line 6"));
        mExamplelist.add(new ExampleItem(R.drawable.ic_android, "Seven", "Line 7"));
        mExamplelist.add(new ExampleItem(R.drawable.ic_audiotrack, "Eight", "Line 8"));
        mExamplelist.add(new ExampleItem(R.drawable.ic_wb_sunny, "Nine", "Line 9"));
    }

    public void setButtons() {
        buttonInsert = findViewById(R.id.button_insert);
        buttonRemove = findViewById(R.id.button_remove);
        editTextInsert = findViewById(R.id.edittext_insert);
        editTextRemove = findViewById(R.id.edittext_remove);

        buttonInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = Integer.parseInt(editTextInsert.getText().toString());
                insertItem(position);
            }
        });


        buttonRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = Integer.parseInt(editTextRemove.getText().toString());
                removeItem(position);
            }
        });
    }
}
